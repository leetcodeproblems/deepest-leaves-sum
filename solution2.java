/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */

//BFS with 2 Queues
 class Solution {
    public int deepestLeavesSum(TreeNode root) {
        
        int ans = 0;
        
        if(root==null) return ans;
        
        Queue<TreeNode> q = new LinkedList<>();
        Queue<TreeNode> p = new LinkedList<>();
        
        q.add(root);
        
        int temp = 0;
        
        while(!q.isEmpty()){
            //front
            TreeNode front= q.remove();
            temp+=front.val;
            
            ///Neighbors
            //check left
            if(front.left!=null){
                p.add(front.left);
            }
            //check right
            if(front.right!=null){
                p.add(front.right);
            }
            
            //level over
            if(q.isEmpty()){
                ans=temp;
                q=p;
                p= new LinkedList<>();
                temp= 0;
            }
        }
        return ans;

    }
}
